import com.dev.Shape;
import com.dev.Circle;
import com.dev.Rectangle;
import com.dev.Square;

public class App {
    public static void main(String[] args) throws Exception {
        // khai báo đối tượng Shape
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("Green", false);

        System.out.println("Shape 1");
        System.out.println(shape1.toString());
        System.out.println("Shape 2");
        System.out.println(shape2.toString());

        // khai báo đối tượng Circle
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle("Red", true, 3.0);

        System.out.println("Circle 1");
        System.out.println(circle1.toString());
        System.out.println("Circle 2");
        System.out.println(circle2.toString());
        System.out.println("Circle 3");
        System.out.println(circle3.toString());

        // khai báo đối tượng rectangle
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.5, 1.5);
        Rectangle rectangle3 = new Rectangle("Pink", true, 2.0, 1.5);

        System.out.println("Rectangle 1");
        System.out.println(rectangle1.toString());
        System.out.println("Rectangle 2");
        System.out.println(rectangle2.toString());
        System.out.println("Rectangle 3");
        System.out.println(rectangle3.toString());

        // khai báo đối tượng square
        Square square1 = new Square();
        Square square2 = new Square(1.5);
        Square square3 = new Square("Black", true, 1.8);

        System.out.println("Square 1");
        System.out.println(square1.toString());
        System.out.println("Square 2");
        System.out.println(square2.toString());
        System.out.println("Square 3");
        System.out.println(square3.toString());

    }
}
