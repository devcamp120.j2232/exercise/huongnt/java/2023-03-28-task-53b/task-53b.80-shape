package com.dev;

public class Rectangle extends Shape {
    double width = 1.0;
    double length = 1.0;

    //khởi tạo phương thức
    public Rectangle() {
    }

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public Rectangle(String color, boolean filled, double width, double length) {
        super(color, filled);
        this.width = width;
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    //phương thức khác
    public double getArea(double width, double length){
        return width*length;
    }

    public double getPerimeter(double width, double length){
        return (width + length)*2;
    }

    @Override
    public String toString() {
        return "Rectangle [Shape[color =" + this.color + ", filled=" + this.filled + "], width=" + width + ", length=" + length + ", Area = " + getArea(this.width, this.length) + ", Perimeter = " + getPerimeter(this.width, this.length) + "]";
    }
    
    
}
