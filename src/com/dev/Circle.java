package com.dev;

public class Circle extends Shape {
    double radius = 1.0;

    //khởi tạo phương thức
    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    
    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }
    
    //phương thức khác
    public double getArea(){
        return Math.PI * Math.pow(this.radius,2);

    }

    public double getPerimeter(){
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString() {
        return "Circle [Shape[color =" + this.color + ", filled=" + this.filled + "], radius=" + radius + ", Area = " + getArea() + ", Perimeter = " + getPerimeter() + "]";
    }

    

}