package com.dev;

public class Square extends Rectangle {
    double side;

    //khởi tạo phương thức
    public Square() {
    }

    public Square(double side) {
        this.side = side;
    }

    public Square(String color, boolean filled, double side) {
        super.color = color;
        super.filled = filled;
        this.side = side;
    }

    //getter and setter
    public double getSide() {
        if (this.side == 0.0){
            side = 1.0;
        }
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    //phương thức khác
    public void setWidth(double side) {
        this.width = side;
    }

    public void setLength(double side) {
        this.length = side;
    }

    @Override
    public String toString() {
        return "Square [Shape[color =" + this.color + ", filled=" + this.filled + "], width=" + getSide() + ", length=" + getSide() + ", Area = " + getArea(this.side, this.side) + ", Perimeter = " + getPerimeter(this.side, this.side) + "]";
    }


    
    
    
}
