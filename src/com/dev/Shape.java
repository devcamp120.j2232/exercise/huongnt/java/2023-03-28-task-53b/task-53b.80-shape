package com.dev;

public class Shape {
    String color = "red";
    boolean filled = true;

    //khởi tạo phương thức
    public Shape() {
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    //getter and setter
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    
    //phương thức khác
    public boolean isFilled(){
        return this.filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString() {
        return "Shape [color= " + color + ", filled= " + filled + "]";
    }

    
}
